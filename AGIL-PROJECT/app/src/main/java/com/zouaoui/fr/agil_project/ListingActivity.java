package com.zouaoui.fr.agil_project;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.ImageButton;

/**
 * Created by zouaoui on 28/01/2017.
 */

public class ListingActivity extends AppCompatActivity{
    ImageButton carte0;
    ImageButton carteFrac12;
    ImageButton carte1;
    ImageButton carte2;
    ImageButton carte3;
    ImageButton carte5;
    ImageButton carte8;
    ImageButton carte13;
    ImageButton carte20;
    ImageButton carte40;
    ImageButton carte100;
    ImageButton carteInfinie;
    ImageButton carteQuestion;
    ImageButton carteBreak;
    Image image;


    Animation zoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_listing);

        initialize();
        bindListenenr();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.aPropos:
                Intent appel = new Intent(ListingActivity.this, APropos.class);
                startActivity(appel);
                return true;
            case R.id.aide:
                Intent appelAide = new Intent(ListingActivity.this, Aide.class);
                startActivity(appelAide);
                return true;
            case R.id.quitter:
                finish();
                System.exit(0);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialize() {
        carte0 = (ImageButton) findViewById(R.id.ImageButton0);
        carteFrac12 = (ImageButton) findViewById(R.id.ImageButtonFrac12);
        carte1 = (ImageButton) findViewById(R.id.ImageButton1);
        carte2 = (ImageButton) findViewById(R.id.ImageButton2);
        carte3 = (ImageButton) findViewById(R.id.ImageButton3);
        carte5 = (ImageButton) findViewById(R.id.ImageButton5);
        carte8 = (ImageButton) findViewById(R.id.ImageButton8);
        carte13 = (ImageButton) findViewById(R.id.ImageButton13);
        carte20 = (ImageButton) findViewById(R.id.ImageButton20);
        carte40 = (ImageButton) findViewById(R.id.ImageButton40);
        carte100 = (ImageButton) findViewById(R.id.ImageButton100);
        carteInfinie = (ImageButton) findViewById(R.id.ImageButtonInfinie);
        carteQuestion = (ImageButton) findViewById(R.id.ImageButtonQuestion);
        carteBreak = (ImageButton) findViewById(R.id.ImageButtonBreak);

    }

    private void bindListenenr() {
        carte0.setOnClickListener(carte0Listener);
        carteFrac12.setOnClickListener(carteFrac12Listener);
        carte1.setOnClickListener(carte1Listener);
        carte2.setOnClickListener(carte2Listener);
        carte3.setOnClickListener(carte3Listener);
        carte5.setOnClickListener(carte5Listener);
        carte8.setOnClickListener(carte8Listener);
        carte13.setOnClickListener(carte13Listener);
        carte20.setOnClickListener(carte20Listener);
        carte40.setOnClickListener(carte40Listener);
        carte100.setOnClickListener(carte100Listener);
        carteInfinie.setOnClickListener(carteInfinieListener);
        carteQuestion.setOnClickListener(carteQuestionListener);
        carteBreak.setOnClickListener(carteBreakListener);

    }

    private View.OnClickListener carte0Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace0.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carteFrac12Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFaceFrac12.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carte1Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace1.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carte2Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace2.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carte3Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace3.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carte5Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace5.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carte8Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace8.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carte13Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace13.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carte20Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace20.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carte40Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace40.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carte100Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFace100.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carteInfinieListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFaceInfinie.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carteQuestionListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFaceQuestion.class);
            startActivity(appel);
        }
    };

    private View.OnClickListener carteBreakListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(ListingActivity.this, BackFaceBreak.class);
            startActivity(appel);
        }
    };
}
