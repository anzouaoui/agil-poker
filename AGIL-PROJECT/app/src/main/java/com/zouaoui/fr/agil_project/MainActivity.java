package com.zouaoui.fr.agil_project;

import android.content.Intent;
import android.graphics.Typeface;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    TextView title;
    Button buttonHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        bindListenenr();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.aPropos:
                Intent appelAPropos = new Intent(MainActivity.this, APropos.class);
                startActivity(appelAPropos);
                return true;
            case R.id.aide:
                Intent appelAide = new Intent(MainActivity.this, Aide.class);
                startActivity(appelAide);
                return true;
            case R.id.quitter:
                finish();
                System.exit(0);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * Initialize components
     */
    private void initialize(){
        title = (TextView) findViewById(R.id.TextViewTitle);
        buttonHome = (Button) findViewById(R.id.ButtonHome);
        setFont(title, "Kalam-Bold.ttf");
    }

    /**
     * Change the font-family
     * @param textView
     * @param fontName
     */
    private void setFont(TextView textView, String fontName) {
        if(fontName != null){
            try {
                Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/" + fontName);
                textView.setTypeface(typeface);
            } catch (Exception e) {
                Log.e("FONT", fontName + " not found", e);
            }
        }
    }

    /**
     * Find Listener
     */
    private void bindListenenr() {
        buttonHome.setOnClickListener(buttonHomeListener);
    }

    private View.OnClickListener buttonHomeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent appel = new Intent(MainActivity.this, ListingActivity.class);
            startActivity(appel);
        }
    };
}
