package com.zouaoui.fr.agil_project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;

/**
 * Created by zouaoui on 06/02/2017.
 */

public class BackFace1  extends AppCompatActivity implements Animation.AnimationListener {
    RelativeLayout backFaceLayout;
    Button buttonReturn;
    RelativeLayout mainLayout;
    Animation animation1;
    Animation animation2;

    private Boolean isBackOfCardShowing = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.back_face_1);
        initialize();
        bindListener();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.aPropos:
                Intent appel = new Intent(BackFace1.this, APropos.class);
                startActivity(appel);
                return true;
            case R.id.aide:
                Intent appelAide = new Intent(BackFace1.this, Aide.class);
                startActivity(appelAide);
                return true;
            case R.id.quitter:
                finish();
                System.exit(0);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initialize() {
        backFaceLayout = (RelativeLayout) findViewById(R.id.RelativeLyoutBackFace);
        buttonReturn = (Button) findViewById(R.id.ButtonReturn);
        mainLayout = (RelativeLayout) findViewById(R.id.RelativeLayoutMain);
        animation1 = AnimationUtils.loadAnimation(this, R.anim.to_middle);
        animation2 = AnimationUtils.loadAnimation(this, R.anim.from_middle);
    }

    private void bindListener() {
        buttonReturn.setOnClickListener(buttonReturnListener);
        animation1.setAnimationListener(this);
        animation2.setAnimationListener(this);
    }

    private View.OnClickListener buttonReturnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v.setEnabled(false);
            backFaceLayout.clearAnimation();
            backFaceLayout.setAnimation(animation1);
            backFaceLayout.startAnimation(animation1);
        }
    };

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == animation1) {
            if (isBackOfCardShowing) {
                backFaceLayout.setBackgroundResource(R.drawable.frontface1);
            }
            else {
                backFaceLayout.setBackgroundResource(R.drawable.backface);
            }
            backFaceLayout.clearAnimation();
            backFaceLayout.setAnimation(animation2);
            backFaceLayout.startAnimation(animation2);
        }
        else {
            isBackOfCardShowing = !isBackOfCardShowing;
            buttonReturn.setEnabled(true);
        }
    }

    @Override
    public void onAnimationRepeat (Animation animation) {
        //TODO Auto-generated method stub
    }

    @Override
    public void onAnimationStart (Animation animation) {
        //TODO Auto-generated method stub
    }
}
